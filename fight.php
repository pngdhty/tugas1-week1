<?php

trait Fight
{
    public $attackPower;
    public $defencePower;
    protected $fight = true;

    use Hewan;

    public function sdarah($darah)
    {
        $this->darah = $darah;
    }

    public function gdarah()
    {
        return $this->darah;
    }

    public function pdarah($d)
    {
        $z = $this->gdarah() - $d->attackPower / $this->defencePower;
        $this->sdarah($z);
        return $this->gdarah();
    }

    public function serang($a)
    {
        $k = $a->gdarah() - $a->pdarah($this);
        return $this->nama . ' Menyerang ' . $a->nama . '<br> Darah ' . $a->nama . ' berkurang : ' . $k . '<br>
        Darah ' . $a->nama . ' tersisa : ' . $a->gdarah() . '<br>';
    }

    public function diserang($s)
    {
        $kurang = $s->gdarah() - $s->pdarah($this);
        return $s->nama . ' Diserang ' . $this->nama . '<br> Darah ' . $s->nama . ' berkurang : ' . $kurang  . '<br>
        Darah ' . $s->nama . ' tersisa : ' . $s->gdarah() . '<br>';
    }

    public function berakhir($e)
    {
        return $this->gdarah() < 1 || $e->gdarah() < 1;
    }

    public function fight($d)
    {
        while (!$this->berakhir($d)) {
            // $this->fight = !$this->fight;
            $r = rand(1, 0);
            if ($r == 1) {
                echo $this->serang($d), '<br>';
            } else {
                echo $d->diserang($this), '<br>';
            }
        }

        if ($this->gdarah() < 0) {
            echo 'CONGRATS!! ' . $d->nama . ' adalah pemenangnya';
        } else {
            echo 'CONGRATS!! ' . $this->nama . ' adalah pemenangnya';
        }
    }
}
