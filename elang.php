<?php

include_once "hewan.php";
include_once "fight.php";

class Elang
{
    use Hewan, Fight;

    protected $jenis_hewan = 'Elang';

    public function __construct()
    {
        $this->nama = 'Nala';
        $this->keahlian = 'terbang tinggi';
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan()
    {
        return 'Jenis Hewan ' . $this->jenis_hewan . ', Bernama ' . $this->nama . ' memiliki jumlah kaki bernilai ' . $this->jumlahKaki = 2 .
            ' dan keahlian bernilai "' . $this->keahlian .
            '", attackPower = ' . $this->attackPower .
            ', deffencePower = ' . $this->defencePower;
    }
}

$elang = new Elang();
